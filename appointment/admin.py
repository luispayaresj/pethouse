from django.contrib import admin

# Register your models here.
from appointment.models import Appointment


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('date','type_service','vet','pet')
    list_filter = ('date','vet','type_service')