from django.db import models
from pet.models import Pet
from vet.models import Vet


# Create your models here.


class Appointment(models.Model):
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
    vet = models.ForeignKey(Vet, on_delete=models.CASCADE)
    type_service = models.CharField(max_length=50)
    date = models.DateTimeField()

    def __str__(self):
        return self.type_service + ' ' + str(self.date)
