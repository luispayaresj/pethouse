from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Client

class ClientForm(forms.ModelForm):
    options = (
        ('female', 'Female'),
        ('male', 'Male')
    )
    YEARS = [x for x in range(1940,2022)]

    gender = forms.CharField(widget=forms.Select(choices=options, attrs={'class': 'form-control'}))
    #password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    birth_day = forms.DateField(label='What is your birth date?', widget=forms.SelectDateWidget (years = YEARS))

    class Meta:
        model = Client
        fields = ['id_client','first_name', 'last_name', 'email', 'phone',
        'user_name', 'password', 'address', 'birth_day', 'gender'
        ]
        