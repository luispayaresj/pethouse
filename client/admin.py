from django.contrib import admin

# Register your models here.

from client.models import Client

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id','id_client','user_name','first_name','last_name','email','phone', 'status_account')
    search_fields = ('id_client','user_name','first_name','last_name')

#admin.site.register(Client)
