from django.shortcuts import render, redirect
from client.models import Client
from .forms import ClientForm
from django.core.exceptions import ObjectDoesNotExist


# Create your views here.



def Login(request):
    return render(request, 'client/login.html', {})


def Home(request):
    """
    You can show the index view in your proyect
    """
    return render(request, 'index.html')


def RegisterClient(request):
    """
    You can register the clients in your database and show the register form.
    """
    if request.method == 'POST':
        client_form = ClientForm(request.POST)
        if client_form.is_valid():
            client_form.save()
            return redirect('clientList')
    else:
        client_form = ClientForm()
    
    context = {
        'client_form': client_form
    }

    return render(request, 'client/register_client.html', context)

def UpdateClient(request, id):
    """
    You can update the clients in your database
    """
    client_form = None
    error = None
    status = None
    try:
        client = Client.objects.get(id = id)
        if client.status_account == True:
            if request.method == 'GET':
                client_form = ClientForm(instance = client)
            else:
                client_form = ClientForm(request.POST, instance = client)
                if client_form.is_valid():
                    client_form.save()
                return redirect('clientList')
        else:
            status = "Client desactivated"
    except ObjectDoesNotExist:
        error = "Client Does not exist"
        
    context = {
        'client_form': client_form,
        'error': error,
        'status': status
    }
    return render(request, 'client/update_client.html', context)


def DeleteClient(request, id):
    """
    Delete a client changing his status account to False
    """
    client = Client.objects.get(id = id)
    client.status_account = False
    client.save()
    return redirect('clientList')



def List_client(request):
    """
    You can show your clients of your database
    """
    client = Client.objects.filter(status_account = True)

    context = {
        'clients': client    
    }

    return render(request, 'client/client_list.html', context)