from django.urls import path
from client import views


urlpatterns = [
    path('login/', views.Login, name='login'),
    path('client/list', views.List_client, name='clientList'),
    path('client/register', views.RegisterClient, name='clientRegister'),
    path('client/update/<int:id>', views.UpdateClient, name='updateClient'),
    path('client/delete/<int:id>', views.DeleteClient, name='deleteClient'),
]