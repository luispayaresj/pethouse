from django.db import models

# Create your models here.

class Client(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_client = models.CharField(max_length=50, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=100, unique=True)
    phone = models.CharField(max_length=50)
    user_name = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    birth_day = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=50)
    status_account = models.BooleanField(default=True)


    def __str__(self):
        return self.first_name + ' ' + self.last_name

    
