from django.contrib import admin

# Register your models here.
from vet.models import Vet

@admin.register(Vet)
class VetAdmin(admin.ModelAdmin):
    list_display = ('id_vet', 'first_name', 'last_name', 'email', 'phone','years_experience','specialization')
    search_fields = ('id_vet','first_name', 'last_name')
    list_filter = ('years_experience','specialization')

#admin.site.register(Vet)