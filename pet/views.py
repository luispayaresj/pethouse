from django.shortcuts import render, redirect
from pet.models import Pet
from .forms import PetForm
from django.core.exceptions import ObjectDoesNotExist
# Create your views here.

def RegisterPet(request):
    """
    You can register the pets in your database and show the register form.
    """
    if request.method == 'POST':
        pet_form = PetForm(request.POST)
        if pet_form.is_valid():
            pet_form.save()
            return redirect('petList')
    else:
        pet_form = PetForm()

    context = {
        'pet_form': pet_form
    }

    return render(request, 'pet/register_pet.html', context)

def UpdatePet(request, id):
    """
    You can update the pets in your database
    """
    pet_form = None
    error = None
    status = None
    try:
        pet = Pet.objects.get(id=id)
        if pet.status_pet:
            if request.method == 'GET':
                pet_form = PetForm(instance=pet)
            else:
                pet_form = PetForm(request.POST, instance=pet)
                if pet_form.is_valid():
                    pet_form.save()
                return redirect('petList')
        else:
            status = "The pet is deactivated in the system"
    except ObjectDoesNotExist:
        error = "Pet Does not exist"

    context = {
        'pet_form': pet_form,
        'error': error,
        'status': status
    }
    return render(request, 'pet/update_pet.html', context)

def DeletePet(request, id):
    """
    Delete a pet changing his status account to False
    """
    pet = Pet.objects.get(id = id)
    pet.status_pet = False
    pet.save()
    return redirect('petList')


def List_pet(request):
    """
    You can show your pets of your database
    """
    pet = Pet.objects.filter(status_pet = True)

    context = {
        'pets': pet
    }

    return render(request, 'pet/pet_list.html', context)