from django.contrib import admin

# Register your models here.
from pet.models import Pet

@admin.register(Pet)
class PetAdmin(admin.ModelAdmin):
    list_display = ('name','breed','age','owner')
    search_fields = ('owner','name','breed')
    list_filter = ('breed','age','weight')
#admin.site.register(Pet)
