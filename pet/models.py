from django.db import models
from client.models import Client


class Pet(models.Model):
    owner = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    age = models.FloatField()
    units = models.CharField(max_length=10, default='years')
    weight = models.FloatField(blank=True,null=True)
    breed = models.CharField(max_length=50)
    status_pet = models.BooleanField(default=True)

    def __str__(self):
        return self.name
