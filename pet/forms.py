from django import forms
from .models import Pet


class PetForm(forms.ModelForm):
    options = (
        ('years', 'Years'),
        ('months', 'Months')
    )

    units = forms.CharField(label='Is age in years or months?',
                            widget=forms.Select(choices=options, attrs={'class': 'form-control'}))
    weight = forms.FloatField(label='Weight must be in kilograms')

    class Meta:
        model = Pet
        fields = ['owner', 'name', 'age', 'units', 'weight',
                  'breed']
