from django.urls import path

from pet import views

urlpatterns = [
    path('register',views.RegisterPet,name='petRegister'),
    path('list', views.List_pet, name='petList'),
    path('update/<int:id>',views.UpdatePet,name='updatePet'),
    path('delete/<int:id>',views.DeletePet, name='deletePet')
]